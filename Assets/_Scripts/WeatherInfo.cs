﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets
{
    [Serializable]
    public class Weather
    {
        public int id;
        public string main;
    }

    [Serializable]
    public class WeatherInfo
    {
        public int id;
        public string name;
        public List<Weather> weather;
        public WeatherMain main;
    }

    [Serializable]
    public class WeatherMain
    {
        public float temp;
    }
}

