﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum TempScale { celsius, fahrenheit};

public class UIManager : MonoBehaviour {

    public Text CityName;
    public Text Temperature;
    public TempScale CurrentScale;

    private const string DEFAULT_CITY = "DefaultCity";
    private const float DEFAULT_TEMP = 0f;
    private const TempScale DEFAULT_SCALE = TempScale.celsius;

    // Use this for initialization
    void Start () {
        SetCity(DEFAULT_CITY);
        CurrentScale = DEFAULT_SCALE;
        SetScale(CurrentScale);
        SetTemperature(DEFAULT_TEMP);

    }
	
    public void SetCity(string newName)
    {
        CityName.text = newName;
    }

    public void SetScale (TempScale newScale)
    {
        if (CurrentScale != newScale)
        {
            float newTemperature;
            float.TryParse(Temperature.text, out newTemperature);
            switch (newScale)
            {
                case TempScale.celsius:
                    newTemperature = (newTemperature - 32) * 5 / 9;
                    break;
                case TempScale.fahrenheit:
                    newTemperature = (newTemperature * 9) / 5 + 32;
                    break;
                default:
                    break;
            }

            Temperature.text = newTemperature.ToString("f1") + "°";
        }
    }

    public void SetTemperature (float newTemperature)
    {
        Temperature.text = newTemperature.ToString("f1") + "°";
    }
}
