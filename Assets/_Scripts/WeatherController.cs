﻿using System.Collections;
using UnityEngine;
using System;
using Assets;
using UnityEngine.Networking;

public class WeatherController : MonoBehaviour {

    private const string API_KEY = "6ec714cc74b2edf329cb21833eb659b0";
    private const float API_CHECK_MAXTIME = 10 * 60f; //10 mins

    public string CityId;
    public GameObject SnowSystem;
    public GameObject RainSystem;
    public UIManager CurrentUIManager;
    public TempScale CurrentScale;

    private float apiCheckCountdown = API_CHECK_MAXTIME;

    // Use this for initialization
    void Start () {
        StartCoroutine(GetWeather(UpdateWeatherDisplay));
	}
	
	// Update is called once per frame
	void Update () {
        apiCheckCountdown -= Time.deltaTime;
        if (apiCheckCountdown <= 0)
        {
            apiCheckCountdown = API_CHECK_MAXTIME;
            StartCoroutine(GetWeather(UpdateWeatherDisplay));
        }
	}

    IEnumerator GetWeather(Action<WeatherInfo> onSuccess)
    {
        using (UnityWebRequest req = UnityWebRequest.Get(String.Format("http://api.openweathermap.org/data/2.5/weather?id={0}&APPID={1}", CityId, API_KEY)))
        {
            yield return req.SendWebRequest();
            while (!req.isDone)
            {
                yield return null;
            }
            byte[] result = req.downloadHandler.data;
            string weatherJSON = System.Text.Encoding.Default.GetString(result);
            WeatherInfo info = JsonUtility.FromJson<WeatherInfo>(weatherJSON);
            onSuccess(info);
        }
    }

    public void UpdateWeatherDisplay(WeatherInfo weatherObj)
    {
        CheckSnowStatus(weatherObj);
        CheckRainStatus(weatherObj);
        ShowTemperature(weatherObj);

        //string CityName = weatherObj.name;

        if (weatherObj.name != CurrentUIManager.CityName.text)
        {
            CurrentUIManager.SetCity(weatherObj.name);
        }

    }

    public void CheckSnowStatus(WeatherInfo weatherObj)
    {
        bool snowing = weatherObj.weather[0].main.Equals("Snow");
        if (snowing)
        {
            SnowSystem.SetActive(true);
        }
        else
        {
            SnowSystem.SetActive(false);
        }
    }

    public void CheckRainStatus(WeatherInfo weatherObj)
    {
        bool snowing = weatherObj.weather[0].main.Equals("Rain");
        if (snowing)
        {
            RainSystem.SetActive(true);
        }
        else
        {
            RainSystem.SetActive(false);
        }
    }

    public void ShowTemperature(WeatherInfo weatherObj)
    {
        float NewTemperature = weatherObj.main.temp;
        Debug.Log("New Kelvin Temperature is: " + weatherObj.main.temp.ToString());

        switch (CurrentUIManager.CurrentScale)
        {
            case TempScale.celsius:
                NewTemperature = NewTemperature - 273.15f;
                break;
            case TempScale.fahrenheit:
                NewTemperature = 1.8f * (NewTemperature - 273.15f) + 32f;
                break;
            default:
                break;
        }

        CurrentUIManager.SetTemperature(NewTemperature);
    }
}
