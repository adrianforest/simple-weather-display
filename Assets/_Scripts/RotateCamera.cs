﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour {

    public float RotateSpeed;
    private const float BASE_ROTATION_SPEED = 0.1f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.Rotate(Vector3.up, (BASE_ROTATION_SPEED * RotateSpeed));
	}
}
